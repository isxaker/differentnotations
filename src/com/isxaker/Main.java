package com.isxaker;

import java.util.UUID;

import static java.util.UUID.randomUUID;

public class Main {

    public static int hex2decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }
        return val;
    }


    // precondition:  d is a nonnegative integer
    public static String decimal2hex(int d) {
        String digits = "0123456789ABCDEF";
        if (d == 0) return "0";
        String hex = "";
        while (d > 0) {
            int digit = d % 16;                // rightmost digit
            hex = digits.charAt(digit) + hex;  // string concatenation
            d = d / 16;
        }
        return hex;
    }

    public static int binlog(int bits) // returns 0 for bits=0
    {
        int log = 0;
        if ((bits & 0xffff0000) != 0) {
            bits >>>= 16;
            log = 16;
        }
        if (bits >= 256) {
            bits >>>= 8;
            log += 8;
        }
        if (bits >= 16) {
            bits >>>= 4;
            log += 4;
        }
        if (bits >= 4) {
            bits >>>= 2;
            log += 2;
        }
        return log + (bits >>> 1);
    }

    public static float max(float[] arr) {
        float max, maxJustNow;
        max = maxJustNow = arr[0];

        for (int i = 1; i < arr.length - 1; i++) {
            maxJustNow = Math.max(maxJustNow, arr[i] + maxJustNow);
            max = Math.max(max, maxJustNow);
        }

        return max;
    }

    public static float find_maximum(float arr[], int n) {
        if (n <= 0) return Float.NaN;

        float max_at = arr[0];  // Maximum value that ends at arr[i]
        float min_at = arr[0];  // Minimum value that ends at arr[i]
        float max_value = max_at;

        for (int i = 1; i < n; i++) {
            float prev_max_at = max_at, prev_min_at = min_at;
            max_at = Math.max(arr[i], Math.max(arr[i] * prev_min_at, arr[i] * prev_max_at));
            min_at = Math.min(arr[i], Math.min(arr[i] * prev_min_at, arr[i] * prev_max_at));
            max_value = Math.max(max_value, max_at);
        }
        return max_value;
    }

    public static void main(String[] args) {


        //UUID testG = randomUUID();
        //long leastSignificantBits = testG.getLeastSignificantBits();
        long instanceCount = 5;
        long hOBit = Long.SIZE - Long.numberOfLeadingZeros(instanceCount);
        //long afterAnd = leastSignificantBits & hOBit;


        int c0 = 0, c1 = 0, c2 = 0, c3 = 0, c4 = 0;
        //long instanceCount = 5;
        //long hOBit = Long.SIZE - Long.numberOfLeadingZeros(instanceCount);
        int res = 0;
        for (int i = 0; i < 1000000; i++) {
            res = 5 & i;
            res = 5 ^ i;
//            UUID g = randomUUID();
//            long leastSignificantBits = g.getLeastSignificantBits();
//
//            long afterAnd = leastSignificantBits & instanceCount;
//
//            if (afterAnd == 0) {
//                c0++;
//            }
//
//            if (afterAnd == 1) {
//                c1++;
//            }
//
//            if (afterAnd == 2) {
//                c2++;
//            }
//
//            if (afterAnd == 3) {
//                c3++;
//            }
//
//            if (afterAnd == 4) {
//                c4++;
//            }


//            switch (g.hashCode() % 5) {
//                case 0: {
//                    c0++;
//                    break;
//                }
//                case 1: {
//                    c1++;
//                    break;
//                }
//                case 2: {
//                    c2++;
//                    break;
//                }
//                case 3: {
//                    c3++;
//                    break;
//                }
//                case 4: {
//                    c4++;
//                    break;
//                }
//            }
        }

        System.out.println("rest c0 = " + c0);
        System.out.println("rest c1 = " + c1);
        System.out.println("rest c2 = " + c2);
        System.out.println("rest c3 = " + c3);
        System.out.println("rest c4 = " + c4);
    }
}
